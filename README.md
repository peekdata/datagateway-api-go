# datagateway-api-go

## Client example of how to access Peekdata Data API Gateway via GO

### Quick usage

```sh
go get gitlab.com/peekdata/datagateway-api-go
```

```go
package main

import (
	"fmt"
	"gitlab.com/peekdata/datagateway-api-go/peekdata"
)

func main() {
	dataRequest := peekdata.GetExampleDataRequest()

	var initializer peekdata.ApiServices
	api := initializer.Init()
	//api.SetupBaseUrl("demo.peekdata.io", 8080, "http") // optional

	dataOptimizedResponse, err := api.GetDataOptimized(dataRequest)

	if err != nil {
		fmt.Println(err.Error())
	} else {
		fmt.Println("GetDataOptimized\n", dataOptimizedResponse)
	}
}

```

Available methods:
- HealthCheck (returns `bool`)
- Validate (returns `bool`)
- GetCSV (returns `string`)
- GetQuery (returns `string`)
- GetCompatibleNodes (returns `*CompatibleNodesResponse`)
- GetData (returns `*DataResponse`)
- GetDataOptimized (returns `*DataOptimizedResponse`)
- SetupBaseUrl

In case of any `error` it will be returned as 2nd argument.

Please consider `main.go` full API usage example.