package main

import (
	"fmt"
	"gitlab.com/peekdata/datagateway-api-go/peekdata"
)

func main() {
	dataRequest := peekdata.GetExampleDataRequest()

	var initializer peekdata.ApiServices
	api := initializer.Init()
	//api.SetupBaseUrl("demo.peekdata.io", 8443, "https") // optional

	status, err := api.HealthCheck()
	if err != nil {
		fmt.Println(err.Error())
	} else {
		fmt.Println("HealthCheck Status:", status)
	}

	validateResponse, err := api.Validate(dataRequest)

	if err != nil {
		fmt.Println(err.Error())
	} else {
		fmt.Println("Validate Status", validateResponse)
	}

	csv, err := api.GetCSV(dataRequest)

	if err != nil {
		fmt.Println(err.Error())
	} else {
		fmt.Println("GetCSV\n", csv)
	}

	queryString, err := api.GetQuery(dataRequest)

	if err != nil {
		fmt.Println(err.Error())
	} else {
		fmt.Println("GetQuery\n", queryString)
	}

	compatibleNodesResponse, err := api.GetCompatibleNodes(dataRequest)

	if err != nil {
		fmt.Println(err.Error())
	} else {
		fmt.Println("GetCompatibleNodes\n", compatibleNodesResponse)
	}

	dataResponse, err := api.GetData(dataRequest)

	if err != nil {
		fmt.Println(err.Error())
	} else {
		fmt.Println("GetData\n", dataResponse)
	}

	dataOptimizedResponse, err := api.GetDataOptimized(dataRequest)

	if err != nil {
		fmt.Println(err.Error())
	} else {
		fmt.Println("GetDataOptimized\n", dataOptimizedResponse)
	}
}
