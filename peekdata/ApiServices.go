package peekdata

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type IApiServices interface {
	SetupBaseUrl(host string, port int, scheme string)
	HealthCheck() (bool, error)
	GetQuery(dataRequest DataRequest) (string, error)
	GetData(dataRequest DataRequest) (*DataResponse, error)
	GetCSV(dataRequest DataRequest) (string, error)
	GetDataOptimized(dataRequest DataRequest) (*DataOptimizedResponse, error)
	GetCompatibleNodes(dataRequest DataRequest) (*CompatibleNodesResponse, error)
	Validate(dataRequest DataRequest) (bool, error)
}

type ApiServices struct {
	IApiServices
	host   string
	port   int
	scheme string
}

const defaultHost = "demo.peekdata.io"
const defaultPort = 8443
const defaultScheme = "https"

func (a *ApiServices) Init() IApiServices {
	a.SetupBaseUrl(defaultHost, defaultPort, defaultScheme)

	return a
}

func (a *ApiServices) SetupBaseUrl(host string, port int, scheme string) {
	a.host = host
	a.port = port
	a.scheme = scheme
}

func getRequest(api ApiServices, dataRequest DataRequest, uri string) (*http.Request, error) {
	result, _ := json.Marshal(dataRequest)
	jsonString := string(result)
	url := fmt.Sprintf("%s://%s:%d/datagateway/rest/v1%s", api.scheme, api.host, api.port, uri)

	var jsonByteString = []byte(jsonString)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonByteString))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")

	return req, nil
}

func doRequest(req *http.Request) (*http.Response, error) {
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

func getPostRequestBody(api ApiServices, dataRequest DataRequest, uri string) (string, error) {
	req, err := getRequest(api, dataRequest, uri)

	if err != nil {
		return "", err
	}

	resp, err := doRequest(req)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	return string(body), nil
}

func (a ApiServices) HealthCheck() (bool, error) {
	url := fmt.Sprintf("%s://%s:%d/datagateway/rest/v1%s", a.scheme, a.host, a.port, "/healthcheck")
	resp, err := http.Get(url)
	if err != nil {
		return false, err
	}

	return resp.StatusCode == 200, nil
}

func (a ApiServices) Validate(dataRequest DataRequest) (bool, error) {
	body, err := getPostRequestBody(a, dataRequest, "/datamodel/validate")

	if err != nil {
		return false, err
	}

	validationResponse := ValidationResponse{}
	json.Unmarshal([]byte(body), &validationResponse)

	return validationResponse.IsValid, nil
}

func (a ApiServices) GetQuery(dataRequest DataRequest) (string, error) {
	body, err := getPostRequestBody(a, dataRequest, "/query/get")
	if err != nil {
		return "", err
	}

	return body, nil
}

func (a ApiServices) GetCSV(dataRequest DataRequest) (string, error) {
	body, err := getPostRequestBody(a, dataRequest, "/file/get")
	if err != nil {
		return "", err
	}
	return body, nil
}

func (a ApiServices) GetCompatibleNodes(dataRequest DataRequest) (*compatibleNodesResponse, error) {
	body, err := getPostRequestBody(a, dataRequest, "/datamodel/getcompatible")
	if err != nil {
		return nil, err
	}
	compatibleNodesResponse := CompatibleNodesResponse{}
	json.Unmarshal([]byte(body), &compatibleNodesResponse)

	return &compatibleNodesResponse, nil
}

func (a ApiServices) GetData(dataRequest DataRequest) (*DataResponse, error) {
	body, err := getPostRequestBody(a, dataRequest, "/data/get")
	if err != nil {
		return nil, err
	}
	dataResponse := DataResponse{}
	json.Unmarshal([]byte(body), &dataResponse)

	return &dataResponse, nil
}

func (a ApiServices) GetDataOptimized(dataRequest DataRequest) (*DataOptimizedResponse, error) {
	body, err := getPostRequestBody(a, dataRequest, "/data/getoptimized")
	if err != nil {
		return nil, err
	}
	dataOptimizedResponse := DataOptimizedResponse{}
	json.Unmarshal([]byte(body), &dataOptimizedResponse)

	return &dataOptimizedResponse, nil
}
