package peekdata

type Node struct {
	Name        string   `json:"name"`
	Title       string   `json:"title"`
	Description string   `json:"description"`
	Groups      []string `json:"groups"`
	DataType    string   `json:"dataType"`
}

type CompatibleNodesResponse struct {
	RequestID  string      `json:"requestID"`
	Message    interface{} `json:"message"`
	ErrorCode  int         `json:"errorCode"`
	Dimensions []Node      `json:"dimensions"`
	Metrics    []Node      `json:"metrics"`
}
