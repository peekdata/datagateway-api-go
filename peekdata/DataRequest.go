package peekdata

type SingleValue struct {
	Keys      []string  `json:"keys"`
	Operation Operation `json:"operation"`
	Value     string    `json:"value"`
}

type SortKey struct {
	Key       string        `json:"key"`
	Direction SortDirection `json:"direction"`
}

type DateRange struct {
	Key  string `json:"key"`
	From string `json:"from"`
	To   string `json:"to"`
}

type SingleKey struct {
	Key       string    `json:"key"`
	Operation Operation `json:"operation"`
	Values    []string  `json:"values"`
}

type Filters struct {
	DateRanges   []DateRange   `json:"dateRanges"`
	SingleKeys   []SingleKey   `json:"singleKeys"`
	SingleValues []SingleValue `json:"singleValues"`
}

type Sortings struct {
	Dimensions []SortKey `json:"dimensions"`
	Metrics    []SortKey `json:"metrics"`
}

type Rows struct {
	LimitRowsTo  int `json:"limitRowsTo"`
	StartWithRow int `json:"startWithRow"`
}

type Options struct {
	Rows      `json:"rows"`
	Arguments interface{} `json:"arguments"`
}

type DTO interface {
	SetDimensions(dimensions []string)
	SetMetrics(metrics []string)
	SetFilters(dateRanges []DateRange, singleKeys []SingleKey, singleValues []SingleValue)
	SetSortings(sortings Sortings)
	SetOptions(options Options)
}

type DataRequest struct {
	DTO
	RequestID    string `json:"requestID"`
	ConsumerInfo string `json:"consumerInfo"`
	ScopeName    string   `json:"scopeName"`
	DataModelName    string   `json:"dataModelName"`
	Dimensions   []string `json:"dimensions"`
	Metrics      []string `json:"metrics"`
	Filters      `json:"filters"`
	Sortings     `json:"sortings"`
	Options      `json:"options"`
	SecurityRole string `json:"securityRole"`
}

func (dataRequest *DataRequest) SetDimensions(dimensions []string) {
	dataRequest.Dimensions = dimensions
}

func (dataRequest *DataRequest) SetMetrics(metrics []string) {
	dataRequest.Metrics = metrics
}

func (dataRequest *DataRequest) SetFilters(dateRanges []DateRange, singleKeys []SingleKey, singleValues []SingleValue) {
	dataRequest.Filters.DateRanges = dateRanges

	dataRequest.Filters.SingleKeys = singleKeys

	dataRequest.Filters.SingleValues = singleValues
}

func (dataRequest *DataRequest) SetSortings(sortings Sortings) {
	dataRequest.Sortings = sortings
}

func (dataRequest *DataRequest) SetOptions(options Options) {
	dataRequest.Options = options
}
