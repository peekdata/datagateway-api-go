package peekdata

type ColumnHeaders struct {
	Title    string      `json:"title"`
	Name     string      `json:"name"`
	DataType string      `json:"dataType"`
	Format   interface{} `json:"format"`
	Alias    string      `json:"alias"`
}

type DataResponse struct {
	RequestID     string                   `json:"requestID"`
	ColumnHeaders []ColumnHeaders          `json:"columnHeaders"`
	Rows          []map[string]interface{} `json:"rows"`
	TotalRows     int                      `json:"totalRows"`
}

type DataOptimizedResponse struct {
	RequestID     string          `json:"requestID"`
	ColumnHeaders []ColumnHeaders `json:"columnHeaders"`
	Rows          [][]interface{} `json:"rows"`
	TotalRows     int             `json:"totalRows"`
}
