package peekdata

func GetExampleDataRequest() DataRequest {
	var dataRequest = DataRequest{}

	dataRequest.ScopeName = "Mortgage-Lending"
	dataRequest.DataModelName = "Servicing-PostgreSQL"
	dataRequest.RequestID = "32693"

	dataRequest.SetDimensions([]string{
		"cityname",
		"currency",
		"countryname",
	})

	dataRequest.SetMetrics([]string{
		"loanamount",
		"propertyprice",
	})

	dateRanges := []DateRange{
		{
			Key:  "closingdate",
			From: "2015-01-01",
			To:   "2017-12-31",
		},
	}

	var op Operation
	op = OperationEquals

	singleKeys := []SingleKey{
		{
			Key:       "propertycityid",
			Operation: op,
			Values: []string{
				"30",
				"35",
				"26",
			},
		},
		{
			Key:       "countryname",
			Operation: op,
			Values: []string{
				"Poland",
			},
		},
	}

	singleValues := []SingleValue{
		{
			Keys: []string{
				"closingdate",
				"approvaldate",
			},
			Operation: op,
			Value:     "2012-12-25",
		},
	}

	dataRequest.SetFilters(dateRanges, singleKeys, singleValues)

	var asc SortDirection
	asc = SortDirectionAsc

	var desc SortDirection
	desc = SortDirectionDesc

	sortings := Sortings{
		Dimensions: []SortKey{
			{
				Key:       "currency",
				Direction: asc,
			},
			{
				Key:       "cityname",
				Direction: desc,
			},
		},
	}

	dataRequest.SetSortings(sortings)

	options := Options{
		Rows: Rows{
			LimitRowsTo:  100,
			StartWithRow: 0, // offset
		},
		Arguments: struct{}{},
	}

	dataRequest.SetOptions(options)

	dataRequest.SecurityRole = ""

	return dataRequest
}
