package peekdata

type Operation int

const (
	OperationEquals           Operation = 0
	OperationNotEquals        Operation = 1
	OperationStartsWith       Operation = 2
	OperationNotStartsWith    Operation = 3
	OperationAllIsLess        Operation = 4
	OperationAllIsMore        Operation = 5
	OperationAtLeastOneIsLess Operation = 6
	OperationAtLeastOneIsMore Operation = 7
)

func (op Operation) String() string {
	arr := [...]string{
		"EQUALS",
		"NOT_EQUALS",
		"STARTS_WITH",
		"NOT_STARTS_WITH",
		"ALL_IS_LESS",
		"ALL_IS_MORE",
		"AT_LEAST_ONE_IS_LESS",
		"AT_LEAST_ONE_IS_MORE",
	}

	return arr[op]
}
