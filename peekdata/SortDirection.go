package peekdata

type SortDirection int

const (
	SortDirectionAsc  SortDirection = 0
	SortDirectionDesc SortDirection = 1
)

func (op SortDirection) String() string {
	arr := [...]string{
		"ASC",
		"DESC",
	}

	return arr[op]
}
