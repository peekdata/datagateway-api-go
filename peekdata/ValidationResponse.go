package peekdata

type ValidationResponse struct {
	RequestID string      `json:"requestID"`
	Message   interface{} `json:"message"`
	ErrorCode int         `json:"errorCode"`
	IsValid   bool        `json:"isValid"`
}
